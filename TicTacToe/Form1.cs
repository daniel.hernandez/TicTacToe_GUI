﻿/*
    Daniel Hernandez
    Tic Tac Toe Game that allows 2 player to play this simple game.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class TicTacToe : Form
    {
        bool playerTurn = true;         //Determines player X if true and O if false.
        int turnCount = 0;              //Keeps count to determine draw

        private const int WM_NCHITTEST = 0x84;
        private const int HT_CLIENT = 0x1;
        private const int HT_CAPTION = 0x2;

        public TicTacToe()
        {
            InitializeComponent();
        }

        //Allows "None" FormBorderStyle window to be moved from top panel
        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            if (m.Msg == WM_NCHITTEST)
                m.Result = (IntPtr)(HT_CAPTION);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void minimizeButton_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void aboutButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Tic Tac Toe\nVersion: 2.0\nAuthor: Daniel Hernandez");
        }

        //Adds depth to about, minimize, and closing button on top panel
        private void display_MouseHover(object sender, EventArgs e)
        {
            if (sender.Equals(minimizeButton))
                minimizeButton.ForeColor = Color.DimGray;
            else if (sender.Equals(closeButton))
                closeButton.ForeColor = Color.DimGray;
            else if (sender.Equals(aboutButton))
                aboutButton.ForeColor = Color.DimGray;
        }

        //Return color to about, minimize, and closing button on top panel
        private void display_MouseLeave(object sender, EventArgs e)
        {
            if (sender.Equals(minimizeButton))
                minimizeButton.ForeColor = Color.WhiteSmoke;
            else if (sender.Equals(closeButton))
                closeButton.ForeColor = Color.WhiteSmoke;
            else if (sender.Equals(aboutButton))
                aboutButton.ForeColor = Color.WhiteSmoke;
        }

        private void button_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if (playerTurn)
                button.Text = "X";
            else
                button.Text = "O";

            playerTurn = !playerTurn;           //Alternates turns
            button.Enabled = false;             //Diables button so to not be pressed again
            turnCount++;
            checkForWinner();                   //Check to see if player has won
        }

        private void checkForWinner()
        {
            bool winner = false;

            //Winning combinations
            if ((A1.Text == A2.Text) && (A2.Text == A3.Text) && (!A1.Enabled))
                winner = true;
            else if ((B1.Text == B2.Text) && (B2.Text == B3.Text) && (!B1.Enabled))
                winner = true;
            else if ((C1.Text == C2.Text) && (C2.Text == C3.Text) && (!C1.Enabled))
                winner = true;

            if ((A1.Text == B1.Text) && (B1.Text == C1.Text) && (!A1.Enabled))
                winner = true;
            else if ((A2.Text == B2.Text) && (B2.Text == C2.Text) && (!A2.Enabled))
                winner = true;
            else if ((A3.Text == B3.Text) && (B3.Text == C3.Text) && (!A3.Enabled))
                winner = true;

            if ((A1.Text == B2.Text) && (B2.Text == C3.Text) && (!A1.Enabled))
                winner = true;
            else if ((A3.Text == B2.Text) && (B2.Text == C1.Text) && (!C1.Enabled))
                winner = true;

            if (winner)
            {
                //String winnerName = "";
                disableButtons();

                if (playerTurn)
                {
                    //winnerName = "O";
                    oScore.Text = (Int32.Parse(oScore.Text) + 1).ToString();
                    ODialog o = new ODialog();
                    o.Show();
                }
                else
                {
                    //winnerName = "X";
                    xScore.Text = (Int32.Parse(xScore.Text) + 1).ToString();
                    xDialog x = new xDialog();
                    x.Show();
                }

            }
            else if (turnCount == 9)
            {
                drawDialog d = new drawDialog();
                d.Show();
            }
        }

        private void disableButtons()
        {
            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }
                catch { }
            }
            resetButton.Enabled = true;
            newGameButton.Enabled = true;
        }

        //Reset board to start a new game
        private void newGameButton_Click(object sender, EventArgs e)
        {
            playerTurn = true;
            turnCount = 0;
            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                }
                catch { }
            }
            resetButton.Text = "RESET";
            newGameButton.Text = "NEW GAME";
        }

        //Reset board and player scores
        private void resetButton_Click(object sender, EventArgs e)
        {
            playerTurn = true;
            turnCount = 0;
            oScore.Text = "0";
            xScore.Text = "0";

            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                }
                catch { }
            }
            resetButton.Text = "RESET";
            newGameButton.Text = "NEW GAME";
        }
    }
}